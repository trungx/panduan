import express from "express";
import {sDeleteExchangeRateByDate} from "../../../shared/domain/core/service/sDeleteExchangeRate"
import {responseError} from "./utils"

export const exchangeRateDeleteHandler = async (
  req: express.Request<{
      date?: string,
  }>,
  res: express.Response
) => {

    try {
        await sDeleteExchangeRateByDate(req.params.date)
        res.sendStatus(200)
    } catch(e) {
        responseError(e, res)
    }
};

import "../../../shared/bootstrap"
import test from 'ava';
import sinon from "sinon"
import {responseError} from "./utils"
import {PError} from "../../../shared/domain/core/error/panduan_error";
import {PErrorVars} from "../../../shared/domain/core/error/var";

const mockResponse = () => {
    const res: any = {};
    res.status = sinon.stub().returns(res);
    res.json = sinon.stub().returns(res);
    res.sendStatus = sinon.stub().returns(res);
    return res;
};

test("responseError() general error", t => {
    const res = mockResponse()

    responseError(new Error("some error from mysql for e.g"), res)
    t.true(res.sendStatus.calledWith(500))

})

test("responseError() logic error", t => {
    const res = mockResponse()

    responseError(new PError(PErrorVars.E105_NO_AFFECTED, "LOGIC"), res)
    t.true(res.status.calledWith(422))
    t.true(res.json.calledWith({
        code: PErrorVars.E105_NO_AFFECTED,
        fields: undefined,
    }))
})

test("responseError() logic error with fields", t => {
    const res = mockResponse()

    responseError(new PError(PErrorVars.E105_NO_AFFECTED, "LOGIC", {
        "sample": "cool"
    }), res)

    t.true(res.status.calledWith(422))
    t.true(res.status().json.calledWith({
        code: PErrorVars.E105_NO_AFFECTED,
        fields: {
            sample: "cool"
        }
    }))
})

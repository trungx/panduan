import express from "express";
import {PError} from "../../../shared/domain/core/error/panduan_error";

export const responseError = (e: Error, res: express.Response) => {
    if (e instanceof PError && e.name === "LOGIC") {
        res.status(422).json({
            code: e.message,
            fields: (e as (PError)).fields,
        })
    } else {
        res.sendStatus(500)
    }

    global.logger.error(`Controller error catch:
    `, e)
}
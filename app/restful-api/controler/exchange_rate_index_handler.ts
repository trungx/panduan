import express from "express";
import {sIndexExchangeRate} from "../../../shared/domain/core/service/sIndexExchangeRate"
import {responseError} from "./utils"

export const exchangeRateIndexHandler = async (
  req: express.Request,
  res: express.Response
) => {

    try {
        await sIndexExchangeRate()
        res.sendStatus(200)
    } catch(e) {
        responseError(e, res)
    }
};

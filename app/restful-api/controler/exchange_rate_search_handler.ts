import express from "express";
import {sSearchExchangeRates} from "../../../shared/domain/core/service/sSearchExchangeRates"
import {responseError} from "./utils"
import {toCents} from "../../../shared/utils/currency";

export const exchangeRateSearchHandler = async (
  req: express.Request<{
      currencyId?: string,
  }, void, void, {
      startdate?: string,
      enddate?: string,
  }>,
  res: express.Response
) => {
    try {
        const exchangeRates = await sSearchExchangeRates(req.query.startdate, req.query.enddate, req.params.currencyId)
        res.status(200).json({
            data: exchangeRates.map(e => ({
                currency_id: e.currency_id,
                source_code: e.source_code,
                beli: toCents(e.buy),
                jual: toCents(e.sell),
            })),
        })
    } catch(e) {
        responseError(e, res)
    }
};

import "../../shared/bootstrap"
import express from "express";
import bodyParser from "body-parser";
import asyncHandler from "express-async-handler";

import { exchangeRateIndexHandler } from "./controler/exchange_rate_index_handler";
import { exchangeRateDeleteHandler } from "./controler/exchange_rate_delete_handler";
import { exchangeRateSearchHandler } from "./controler/exchange_rate_search_handler";
import { exchangeRateCreateHandler } from "./controler/exchange_rate_create_handler";
import { exchangeRateUpdateHandler } from "./controler/exchange_rate_update_handler";

const app = express();

app.get("/api/indexing", asyncHandler(exchangeRateIndexHandler));
app.get("/api/kurs/", asyncHandler(exchangeRateSearchHandler));
app.get("/api/kurs/:currencyId", asyncHandler(exchangeRateSearchHandler));
app.post("/api/kurs/", bodyParser.json(), asyncHandler(exchangeRateCreateHandler));
app.put("/api/kurs/", bodyParser.json(), asyncHandler(exchangeRateUpdateHandler));
app.delete("/api/kurs/:date", asyncHandler(exchangeRateDeleteHandler));

app.listen(process.env.PORT, () => {
  global.logger.info(`server running on ${process.env.PORT}`);
});

export default app
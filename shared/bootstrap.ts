import dotenv from "dotenv";
dotenv.config({
    path: process.env.NODE_ENV ? `.${process.env.NODE_ENV}.env` : ".env",
});

import db from "./config/db"
import {Pool} from "mysql"
import {PError} from "./domain/core/error/panduan_error";
import winston from "winston";
import logger from "../shared/config/log"
declare global {
    namespace NodeJS {
        interface Global {
            db: Pool,
            PError: typeof PError,
            logger: winston.Logger
        }
    }
}
global.db = db
global.PError = PError
global.logger = logger
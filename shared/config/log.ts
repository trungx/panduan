import winston, {LoggerOptions} from "winston";

const config: {
    [env: string]: LoggerOptions
} = {
    test: {
        format: winston.format.simple(),
        transports: [
            (function () {
                const c = new winston.transports.Console();
                c.silent = true
                return c
            })(),
        ],
    },
    other: {
        format: winston.format.combine(
            winston.format.colorize(),
            winston.format.simple()),
        transports: [
            new winston.transports.Console(),
        ],
    }
}

const logger = winston.createLogger(
    config[process.env.NODE_ENV as string] ? config[process.env.NODE_ENV as string] : config.other);

export default logger

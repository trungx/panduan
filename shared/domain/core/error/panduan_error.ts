type PanduanErrorName = "LOGIC" | "INTERNAL" | "INTEGRATION"

export class PError implements Error {
    public name: string
    public message: string
    public fields?: object
    constructor (message: string, name: PanduanErrorName = "INTERNAL", fields?: object) {
        Error.apply(this, [message]); // to get stack involved
        this.name = name
        this.message = message
        this.fields = fields
    }
}
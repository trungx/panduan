export const cleanupDB = async (): Promise<void> => {
    return new Promise(function (resolve, reject){
        global.db.query(`
delete from archived_exchange_rate;
delete from exchange_rate;
`, [], function (e) {
            if (e) {
                reject(e);
                return
            }

            resolve()
        })
    })
}
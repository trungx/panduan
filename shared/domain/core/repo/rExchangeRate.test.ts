import "../../../bootstrap"
import test from "ava";
import sinon from "sinon";
import mysql from "mysql"
import {rListExchangeRateSources} from "./rExchangeRate";
import {sampleERSource} from "../../../fixtures/ersource";

// test("rListExchangeRateSources()", async t => {
//     const sandbox = sinon.createSandbox()
//     const stub = sandbox.stub()
//     const mock = {
//         query:  stub,
//     } as any
//
//     stub.callsArgWith(2, null, sampleERSource)
//     await rListExchangeRateSources(mock)
//         .then((res) => {
//             t.deepEqual(res, sampleERSource)
//         })
//         .catch(e => {
//             t.fail()
//         })
// })

test.failing("rListExchangeRateSources() 2", async t => {
    const sandbox = sinon.createSandbox()
    const stub = sandbox.stub()
    const mock = {
        query:  stub,
    } as any

    stub.callsArgWith(2, new Error("Some database error"))
    t.throws( async () => {
        await rListExchangeRateSources(mock)
    }, { instanceOf: Error} )
})
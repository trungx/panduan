import {Pool, PoolConnection} from "mysql";
import {RepoLock} from "../const";

export const rListExchangeRateSources = async (
    db: Pool | PoolConnection,
): Promise<Array<{
    id: number,
    name: string,
    code: string,
}>> => {
    return new Promise(function (resolve, reject) {
        db.query(`select id, name, code from exchange_rate_source`, [],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }
                resolve(results)
            }
        )
    })
}

export const rInsertExchangeRate = async (
    db: Pool | PoolConnection,
    currencyId: string,
    exchangeRateSourceId: number,
    buy: number,
    sell: number,
    dateStr: string,
): Promise<number> => {
    return new Promise(function (resolve, reject) {
        db.query(`INSERT INTO public.exchange_rate (created_at, currency_id, exchange_rate_source_id, buy, sell, for_date) 
VALUES (now(), ?, ?, ?, ?, ?) ON DUPLICATE KEY UPDATE id=id;`,
            [currencyId, exchangeRateSourceId, buy, sell, dateStr],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }

                resolve(results.insertId)
            }
        )
    })
}

export const rUpdateExchangeRate = async (
    db: Pool | PoolConnection,
    currencyId: string,
    exchangeRateSourceId: number,
    buy: number,
    sell: number,
    dateStr: string,
): Promise<number> => {
    return new Promise(function (resolve, reject) {
        db.query(`
UPDATE public.exchange_rate
set  buy = ?, sell = ?, updated_at = now()
where currency_id = ? 
  and exchange_rate_source_id = ?
  and for_date = ?
`,
            [buy, sell, currencyId, exchangeRateSourceId, dateStr],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }

                resolve(results.affectedRows)
            }
        )
    })
}

export const rDeleteExchangeRatesByDate = (
    db: Pool | PoolConnection,
    dateStr: string
): Promise<void> => {
    return new Promise(function (resolve, reject) {
        db.query(`
delete from exchange_rate
where for_date = ?`,
            [dateStr],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }
                resolve()
            }
        )
    })
}

export const rArchiveExchangeRatesByDate = (
    db: Pool | PoolConnection,
    dateStr: string
): Promise<void> => {
    return new Promise(function (resolve, reject) {
        db.query(`

insert into public.archived_exchange_rate(
 id, deleted_at, created_at, updated_at, currency_id, exchange_rate_source_id, buy, sell, for_date
)
select id, now(), created_at, updated_at, currency_id, exchange_rate_source_id, buy, sell, for_date from
public.exchange_rate
where for_date = ?`,
            [dateStr],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }
                resolve()
            }
        )
    })
}

export const rGetExchangeRatesByConditions = (
    db: Pool | PoolConnection,
    startDateStr?: string,
    endDateStr?: string,
    currencyId?: string,
    sourceCode?: string,
): Promise<Array<{
    currency_id: string,
    source_code: string,
    buy: number,
    sell: number,
}>> => {
    return new Promise(function (resolve, reject){
        db.query(`
select er.currency_id,
       er.buy,
       er.sell,
       ers.code as source_code
from exchange_rate er
    join exchange_rate_source ers on er.exchange_rate_source_id = ers.id
where 1=1
    and er.deleted_at is null
    and (? = false or ? = true and ? <= er.for_date)
    and (? = false or ? = true and er.for_date <= ?)
    and (? = false or ? = true and er.currency_id = ?)
    and (? = false or ? = true and ers.code = ?)
order by er.for_date, er.currency_id, source_code
`, [
    !!startDateStr, !!startDateStr, startDateStr,
    !!endDateStr, !!endDateStr, endDateStr,
    !!currencyId, !!currencyId, currencyId,
    !!sourceCode, !!sourceCode, sourceCode,
],
            function (err, results) {
                if (err) {
                    reject(err)
                    return
                }
                resolve(results)
            }
        )
    })
}

export const acquireExchangeRateLock = async (
    db: Pool | PoolConnection
): Promise<1 | 0 | null> => {
    return new Promise(function (resolve, reject){
        db.query(`
SELECT GET_LOCK('${RepoLock.UPDATE_EXCHANGE_RATE}', 0) as locked;
`, [], function (err, results) {
                if (err) {
                    reject(err)
                    return
                }

                if (results[0]) {
                    resolve(results[0].locked)
                } else {
                    resolve(null)
                }
            }
        )
    })
}

export const releaseExchangeRateLock = async (
    db: Pool | PoolConnection
): Promise<1 | 0 | null> => {
    return new Promise(function (resolve, reject){
        db.query(`
SELECT RELEASE_LOCK('${RepoLock.UPDATE_EXCHANGE_RATE}') as locked;
`, [], function (err, results) {
                if (err) {
                    reject(err)
                    return
                }

                if (results[0]) {
                    resolve(results[0].locked)
                } else {
                    resolve(null)
                }
            }
        )
    })
}
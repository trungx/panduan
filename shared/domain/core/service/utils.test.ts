import test from "ava";
import {validateExchangeRateObject} from "./utils"
import {PErrorVars} from "../error/var";
import {sampleERSource} from "../../../fixtures/ersource";

test("validateExchangeRateObject()", t => {
    t.deepEqual(validateExchangeRateObject({
        symbol: "SGD1",
        date: "2021-05-17ddd",
    } as any, sampleERSource), {
        sources:  PErrorVars.E107_MISSING_SOURCE,
        date:  PErrorVars.E101_INVALID_DATE,
        symbol:  PErrorVars.E102_INVALID_CURRENCY,
    })

    t.deepEqual(validateExchangeRateObject({
        symbol: "SGD",
        date: 1000,
        e_rate: {
            "jual": "invalid",
            "beli": "invalid",
        },
        e_rate1: {
            "jual": "invalid",
            "beli": "invalid",
        }
    } as any, sampleERSource), {
        "e_rate.jual":  PErrorVars.E103_INVALID_NUMBER,
        "e_rate.beli":  PErrorVars.E103_INVALID_NUMBER,
        "e_rate1":  PErrorVars.E104_INVALID_SOURCE_CODE,
        "date": PErrorVars.E101_INVALID_DATE,
    })
})
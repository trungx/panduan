import {
    rGetExchangeRatesByConditions
} from "../repo/rExchangeRate";
import moment from "moment"
import {PError} from "../error/panduan_error";
import {PErrorVars} from "../error/var"
import {validateDate} from "./utils";

export async function sSearchExchangeRates(startDateStr?: string, endDateStr?: string, currencyId?: string) {
    try {

        if (startDateStr !== undefined && startDateStr !== "") {
            if (!validateDate(startDateStr)) {
                throw new PError(PErrorVars.E100_MULTIPLE_ERRORS, "LOGIC", {
                    "startddate": PErrorVars.E101_INVALID_DATE,
                })
            }
        }
        if (endDateStr !== undefined && endDateStr !== "") {
            if (!validateDate(endDateStr)) {
                throw new PError(PErrorVars.E100_MULTIPLE_ERRORS, "LOGIC", {
                    "endddate": PErrorVars.E101_INVALID_DATE,
                })
            }
        }

        return await rGetExchangeRatesByConditions(global.db, startDateStr, endDateStr, currencyId)
    } catch (e) {
        throw e
    }
}
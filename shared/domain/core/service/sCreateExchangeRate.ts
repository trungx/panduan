import {
    rGetExchangeRatesByConditions,
    rInsertExchangeRate, rListExchangeRateSources,
} from "../repo/rExchangeRate";
import {PErrorVars} from "../error/var"
import {PError} from "../error/panduan_error"
import _find from "lodash/find"
import {amountStringToInt} from "../../../utils/currency";
import {ExchangeRateParams, validateExchangeRateObject} from "./utils";
import {dbBegin, dbCommit, dbRollback, getConnection} from "../../../config/db";

export const sCreateExchangeRate = async (
    exchangeRate: ExchangeRateParams
): Promise<{
    [sourceCode: string]: {
        "jual"?: number, // sell, in decimal
        "beli"?: number  // buy, in decimal
    }
} | undefined > => {

    const erSources = await rListExchangeRateSources(global.db)

    const errMap = validateExchangeRateObject(exchangeRate, erSources)
    if (Object.keys(errMap).length > 0) {
        throw new PError(PErrorVars.E100_MULTIPLE_ERRORS, "LOGIC", errMap)
    }

    const addedSources: {
        [sourceCode: string]: {
            "jual"?: number, // sell, in decimal
            "beli"?: number  // buy, in decimal
        }
    } = {}

    const {symbol, date, ...sources} = exchangeRate
    const pool = await getConnection(global.db)

    try {
        await dbBegin(pool)
        for (const sourceCode in sources) {
            if (sources.hasOwnProperty(sourceCode)) {
                const currencyId = symbol.toUpperCase()
                const exchangeRates = await rGetExchangeRatesByConditions(global.db, date, date, currencyId, sourceCode)
                if (exchangeRates && exchangeRates.length > 0) {
                    continue
                }

                const insertedId = await rInsertExchangeRate(global.db,
                    currencyId,
                    (_find(erSources, {code: sourceCode}) as any).id, // cant be undefined for sure. checked above alr. So we can bypass typescript by using "any"
                    amountStringToInt(sources[sourceCode].beli as number),
                    amountStringToInt(sources[sourceCode].jual as number),
                    date)

                if (insertedId > 0) {
                    addedSources[sourceCode] = sources[sourceCode]
                }
            }
        }

        await dbCommit(pool)
        return addedSources
    } catch (e) {
        await dbRollback(pool)
        throw e
    } finally {
        pool.release()
    }

}
import "../../shared/bootstrap"
import test from "ava";
import sinon from "sinon";
import {doHttp} from "./httpClient";

test("doHttp()", t => {
  const sandbox = sinon.createSandbox()
  const stub = sandbox.stub(doHttp as any, "fetchLib")

  doHttp({
    serviceName: "crawl_exchange_rate_informasi"
  })
  t.true(stub.calledWith("https://www.bca.co.id/en/informasi/kurs", {
    method: "GET"
  }))

  t.throws(() => {
    doHttp({
      serviceName: "invalid_service"
    })
  }, { instanceOf: Error })

  doHttp({
    serviceName: "crawl_exchange_rate_informasi",
    query: {
      foo: "bar"
    }
  })
  t.true(stub.calledWith("https://www.bca.co.id/en/informasi/kurs?foo=bar", {
    method: "GET"
  }))

  doHttp({
    serviceName: "sample_service",
    params: {
      param_name: "bar"
    }
  })
  t.true(stub.calledWith("https://www.bca.co.id/en/sample/bar", {
    method: "GET"
  }))

  sandbox.restore()
})

import _isUndefined from "lodash/isUndefined";
import * as queryStringLib from "query-string";
import _get from "lodash/get";
import fetch from "node-fetch"
import sharedServices from "../config/services";
import {Services} from "../types/services";

type ServiceInfo = {
  method: string;
  url: string;
};

let services: Services = [];

const injectServices = (s: Array<{
  base_url: string,
  name: string,
  services: {
    [key: string]: string
  }
}>) => {
  services = [...services, ...s]
}

injectServices(sharedServices)

const getServiceInfo = (args: {
  params?: object;
  query?: object;
  /** E.g: crawl_exchange_rate_informasi. See in List.ts */
  serviceName: keyof typeof services[number]["services"];
}): ServiceInfo => {

  for (const service of services) {
    if (service.services.hasOwnProperty(args.serviceName)) {
      const [method, uri] = service.services[args.serviceName].split(" ");
      let url: string = `${service.base_url}${uri}`;

      if (!_isUndefined(args.query)) {
        url = `${url}?${queryStringLib.stringify(args.query as object)}`;
      }

      if (!_isUndefined(args.params)) {
        Object.keys(args.params).map(key => {
          url = url.replace(`:${key}`, _get(args.params as object, key));
        });
      }

      return {
        method: method.toUpperCase(),
        url
      };
    }
  }

  throw new Error("Service not found " + args.serviceName);
}

const doHttp = (args: {
  params?: object;
  query?: object;
  /** E.g: crawl_exchange_rate_informasi. See in List.ts */
  serviceName: keyof typeof services[number]["services"];
}) => {
  const serviceInfo = getServiceInfo(args)
  return doHttp.fetchLib(serviceInfo.url, {
    method: serviceInfo.method,
  })
}

doHttp.fetchLib = fetch

export {
  doHttp
}